package com.example.testassignment.network;

import com.example.testassignment.db.PictureRepository;
import com.example.testassignment.db.entity.PictureEntity;
import com.example.testassignment.network.response.PictureResponse;
import com.example.testassignment.util.Constants;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiRepository {
    private static ApiRepository INSTANCE = new ApiRepository();

    private final ApiService apiService;

    private ApiRepository() {


        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(interceptor);
        OkHttpClient client = builder.build();
        Retrofit retrofit = new Retrofit.Builder()

                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .baseUrl(Constants.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

                .client(client)
                .build();
        apiService = retrofit.create(ApiService.class);


    }

    public Observable<PictureResponse> loadPictures(String type){
        final Observable<PictureResponse> data = apiService.getData(type);
        return makeAsync(data);
    }

    private <T> Observable<T> makeAsync(Observable<T> observable) {
        return observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }


    public static ApiRepository getInstance() {
        return INSTANCE;
    }
}
