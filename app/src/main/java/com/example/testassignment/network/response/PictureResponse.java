package com.example.testassignment.network.response;

import com.example.testassignment.db.entity.PictureEntity;

import java.util.List;

public class PictureResponse {
    private String message;
    private List<PictureEntity> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PictureEntity> getData() {
        return data;
    }

    public void setData(List<PictureEntity> data) {
        this.data = data;
    }
}
