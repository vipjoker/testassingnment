package com.example.testassignment.network;


import com.example.testassignment.db.PictureRepository;
import com.example.testassignment.network.response.PictureResponse;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {


    @GET("api.php")
    Observable<PictureResponse> getData(@Query("query") String query);
}
