package com.example.testassignment.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.testassignment.db.dao.PictureDao;
import com.example.testassignment.db.entity.PictureEntity;

@Database(version = 1, exportSchema = false,
        entities = {
                PictureEntity.class
        })

public abstract class AppDatabase extends RoomDatabase {
    public static final String DATABASE_NAME = "picture_db";

    public abstract PictureDao getPictureDao();

    public static AppDatabase createDb(Context context) {

        AppDatabase sInstance = Room
                .databaseBuilder(context, AppDatabase.class, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();
        return sInstance;
    }

}
