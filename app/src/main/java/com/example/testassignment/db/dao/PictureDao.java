package com.example.testassignment.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.testassignment.db.entity.PictureEntity;

import java.util.List;

import javax.sql.DataSource;


@Dao
public abstract class PictureDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(PictureEntity pictureEntity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(List<PictureEntity> pictureEntities);

    @Query("DELETE FROM picture WHERE url = :url")
    public abstract void deleteByID(String url);

    @Query("DELETE FROM picture ")
    public abstract void deleteAll();

    @Query("SELECT * FROM picture WHERE type = :type")
    public abstract LiveData<List<PictureEntity>> findAllByType(String type);

    @Query("SELECT * FROM picture WHERE url = :url LIMIT 1")
    public abstract LiveData<PictureEntity> findById(String url);

    @Query("SELECT count(*) FROM picture")
    public abstract LiveData<Integer> getCount();
}
