package com.example.testassignment.db;

import androidx.lifecycle.LiveData;

import com.example.testassignment.db.dao.PictureDao;
import com.example.testassignment.db.entity.PictureEntity;
import com.example.testassignment.util.AppExecutors;

import java.util.List;

public class PictureRepository {

    private final PictureDao mDao;

    private static PictureRepository INSTANCE;

    public static PictureRepository getInstance() {
        if (INSTANCE == null) {
            throw new RuntimeException("You should call PictureRepository.init(PictureDao) first");
        }
        return INSTANCE;
    }

    private PictureRepository(PictureDao dao) {
        this.mDao = dao;
    }

    public static void init(PictureDao dao) {
        INSTANCE = new PictureRepository(dao);
    }

    public void saveAll(List<PictureEntity> pictureEntityList, String type) {
        for (PictureEntity pictureEntity : pictureEntityList) {
            pictureEntity.setType(type);
        }
        AppExecutors.getInstance().diskIO().execute(() -> mDao.insert(pictureEntityList));

    }

    public LiveData<List<PictureEntity>> findAllByType(String type) {
        return mDao.findAllByType(type);
    }

    public LiveData<PictureEntity> findById(String id) {
        return mDao.findById(id);
    }

}
