package com.example.testassignment.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.testassignment.R;
import com.example.testassignment.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding b;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        b = DataBindingUtil.setContentView(this,R.layout.activity_main);
        b.bottomNavigation.setOnNavigationItemSelectedListener(this::onItemClicked);
    }

    private boolean onItemClicked(MenuItem menuItem) {

        final int itemId = menuItem.getItemId();
        if(itemId == b.bottomNavigation.getSelectedItemId()){
            return false;
        }
        NavController navController = Navigation.findNavController(this, R.id.nav_fragment);
        switch (itemId) {
            case R.id.menu_first:
                menuItem.setChecked(true);

                navController.navigate(R.id.action_nav_main_first);
                return true;


            case R.id.menu_second:
                menuItem.setChecked(true);
                navController.navigate(R.id.action_nav_main_second);
                return true;

        }
        return false;
    }
}
