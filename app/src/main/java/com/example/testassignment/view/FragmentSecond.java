package com.example.testassignment.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.testassignment.NavMainDirections;
import com.example.testassignment.databinding.FragmentSecondBinding;
import com.example.testassignment.db.entity.PictureEntity;
import com.example.testassignment.util.Constants;

import java.util.List;

public class FragmentSecond extends BaseFragment {

    private FragmentSecondBinding b;
    private PicturesAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(b == null) {
            b = FragmentSecondBinding.inflate(inflater, container, false);

            setupRecyclerView();
            viewModel.listenForNewPictures(Constants.QUERY_DOG).observe(this, this::onDataLoaded);
            b.toolbar.setTitle(Constants.QUERY_DOG.toUpperCase());
        }
        return b.getRoot();
    }

    private void onDataLoaded(List<PictureEntity> pictureEntities) {
        if(mAdapter != null){
            mAdapter.setItems(pictureEntities);
        }
    }

    private void setupRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mAdapter = new PicturesAdapter(this::onItemClicked);
        b.recyclerView.setLayoutManager(linearLayoutManager);
        b.recyclerView.setAdapter(mAdapter);
    }

    private void onItemClicked(PictureEntity pictureEntity) {


        final NavMainDirections.ActionNavMainDetail actionNavMainDetail = FragmentSecondDirections.actionNavMainDetail(pictureEntity.getUrl());
        Navigation.findNavController(getView()).navigate(actionNavMainDetail);
    }
}
