package com.example.testassignment.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testassignment.databinding.ItemPictureBinding;
import com.example.testassignment.db.entity.PictureEntity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class PicturesAdapter extends RecyclerView.Adapter<PicturesAdapter.PictureHolder> {

    private final List<PictureEntity> mList = new ArrayList<>();
    private ItemClickListener mListener;

    public PicturesAdapter(ItemClickListener listener){
        this.mListener = listener;
    }
    @NonNull
    @Override
    public PictureHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemPictureBinding binding = ItemPictureBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false);

        return new PictureHolder(binding);
    }

    public void setItems(List<PictureEntity> items){
        mList.clear();
        mList.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull PictureHolder holder, int position) {
        final PictureEntity pictureEntity = mList.get(position);
        holder.bind(pictureEntity);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class PictureHolder extends RecyclerView.ViewHolder {
        private final ItemPictureBinding b;

        public PictureHolder(@NonNull ItemPictureBinding binding) {
            super(binding.getRoot());
            this.b = binding;
        }

        public void bind(PictureEntity pictureEntity) {
            Picasso.get().load(pictureEntity.getUrl()).into(b.ivLogo);
            b.tvTitle.setText(pictureEntity.getTitle());
            itemView.setOnClickListener(v-> mListener.onItemClicked(pictureEntity));
        }
    }

    public interface ItemClickListener{
        void onItemClicked(PictureEntity entity);
    }
}
