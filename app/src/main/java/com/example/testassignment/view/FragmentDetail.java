package com.example.testassignment.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.testassignment.R;
import com.example.testassignment.databinding.FragmentDetailBinding;
import com.example.testassignment.db.entity.PictureEntity;
import com.squareup.picasso.Picasso;

public class FragmentDetail extends BaseFragment {
    private FragmentDetailBinding b;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        b = FragmentDetailBinding.inflate(inflater,container,false);
        loadData();
        setupToolBar();
        return b.getRoot();
    }

    private void setupToolBar() {
        b.toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_black_24dp);
        b.toolbar.setNavigationOnClickListener(this::onBackClicked);
    }

    private void onBackClicked(View view) {
        Navigation.findNavController(view).navigateUp();
    }

    private void loadData() {
        final String id = FragmentDetailArgs.fromBundle(getArguments()).getId();
        viewModel.listenForEntity(id).observe(this,this::onLoaded);
    }

    private void onLoaded(PictureEntity pictureEntity) {
        Picasso.get().load(pictureEntity.getUrl()).into(b.ivPicture);
        b.tvTitle.setText(pictureEntity.getTitle());
    }
}
