package com.example.testassignment.model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import io.reactivex.disposables.Disposable;

import com.example.testassignment.db.PictureRepository;
import com.example.testassignment.db.entity.PictureEntity;
import com.example.testassignment.network.ApiRepository;
import com.example.testassignment.network.response.PictureResponse;

import java.util.List;

public class MainViewModel extends AndroidViewModel {

    MediatorLiveData<List<PictureEntity>> listMediatorLiveData = new MediatorLiveData<>();

    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<List<PictureEntity>> listenForNewPictures(String type) {

        LiveData<List<PictureEntity>> allByType = PictureRepository.getInstance().findAllByType(type);

        listMediatorLiveData.addSource(allByType, list -> {

            if (list != null && !list.isEmpty()) {
                listMediatorLiveData.setValue(list);
            } else {
                requestDataFromServer(type);
            }

        });
        return listMediatorLiveData;
    }

    public LiveData<PictureEntity> listenForEntity(String id){
       return PictureRepository.getInstance().findById(id);
    }



    private void requestDataFromServer(String type) {
        final Disposable subscribe = ApiRepository
                .getInstance()
                .loadPictures(type)
                .subscribe(res->onPicturesSuccess(res,type), this::onPicturesFailure);
    }

    private void onPicturesFailure(Throwable throwable) {
        throwable.printStackTrace();
    }

    private void onPicturesSuccess(PictureResponse pictureResponse,String type) {
        PictureRepository.getInstance().saveAll(pictureResponse.getData(),type);
    }


}
