package com.example.testassignment;

import android.app.Application;

import androidx.room.Room;

import com.example.testassignment.db.AppDatabase;
import com.example.testassignment.db.PictureRepository;

public class PicturesApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        AppDatabase build = Room.databaseBuilder(this, AppDatabase.class, AppDatabase.DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();
        PictureRepository.init(build.getPictureDao());
    }
}
