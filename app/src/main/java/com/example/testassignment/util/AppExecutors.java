package com.example.testassignment.util;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class AppExecutors {

    private static final String TAG = AppExecutors.class.getSimpleName();

    private Executor diskIO;
    private Executor networkIO;
    private Executor mainThread;


    private static AppExecutors INSTANCE = new AppExecutors();

    public synchronized static AppExecutors getInstance() {
        return INSTANCE;
    }

    private AppExecutors() {
        this(Executors.newFixedThreadPool(3),
                Executors.newFixedThreadPool(3),
                new MainThreadExecutor());
    }





    private AppExecutors(Executor diskIO, Executor networkIO, Executor mainThread) {
        this.diskIO = diskIO;
        this.networkIO = networkIO;
        this.mainThread = mainThread;
    }

    public Executor diskIO() {
        return diskIO;
    }

    public Executor networkIO() {
        return networkIO;
    }

    public Executor mainThread() {
        return mainThread;
    }

    private static class MainThreadExecutor implements Executor {
        private Handler mainThreadHandler = new Handler(Looper.getMainLooper());

        @Override
        public void execute(Runnable command) {
            mainThreadHandler.post(command);
        }
    }


}
